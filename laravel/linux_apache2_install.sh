#!/bin/bash
# Environment settings: http://vaguelyuseful.info/2016/08/03/installing-laravel-5-2-on-ubuntu-16-04-and-apache2/
# Example usage: bash linux_apache2_install.sh "Git repository url" "Folder name where git code will be cloned" "Local project url"

DIR="$(dirname "$0")"
BASE_PROJECT_DIR="/var/www/html/"
GIT_REPO_URL="$1"
GIT_REPO_DIR_NAME="$2"
LOCAL_PROJECT_URL="$3"
BASE_PROJECT_DIR_USER_GROUP="www-data"
APACHE_CONF_DIR="/etc/apache2/sites-available/"
HOSTS_FILE="/etc/hosts"

source "$DIR/../utils/utils.sh"

if [ -z "$GIT_REPO_URL" ]; then
    exit_with_message_and_code 1 "Git repo url was not supplied"
fi

if [ -z "$GIT_REPO_DIR_NAME" ]; then
    exit_with_message_and_code 2 "Git repo folder name was not supplied"
fi

if [ -z "$LOCAL_PROJECT_URL" ]; then
    exit_with_message_and_code 3 "Local project url was not supplied"
fi

GIT_REPO_DIR="$BASE_PROJECT_DIR$GIT_REPO_DIR_NAME"
if [ -d "$GIT_REPO_DIR" ]; then
    exit_with_message_and_code 4 "Git repo directory $GIT_REPO_DIR already exists"
fi

git clone "$GIT_REPO_URL" "$GIT_REPO_DIR"
check_status_and_exit_if_error 5 "Could not clone $GIT_REPO_URL into $GIT_REPO_DIR"

composer install -d "$GIT_REPO_DIR"
check_status_and_exit_if_error 6 "Could composer install $GIT_REPO_DIR"

sudo chgrp -R "$BASE_PROJECT_DIR_USER_GROUP" "$GIT_REPO_DIR/storage"
sudo chgrp -R "$BASE_PROJECT_DIR_USER_GROUP" "$GIT_REPO_DIR/bootstrap/cache"
check_status_and_exit_if_error 6 "Could not update user group permissions"

PROJECT_APACHE_CONF_SITE="$LOCAL_PROJECT_URL.conf"
PROJECT_APACHE_CONF_FILE="$APACHE_CONF_DIR$PROJECT_APACHE_CONF_SITE"
sudo tee -a "$PROJECT_APACHE_CONF_FILE" <<EOF
<VirtualHost *:80>

        ServerName "$LOCAL_PROJECT_URL"
        DocumentRoot "$GIT_REPO_DIR/public"

        <Directory "$GIT_REPO_DIR">
                AllowOverride All
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOF
check_status_and_exit_if_error 7 "Could not create project apache config file $PROJECT_APACHE_CONF_FILE"

sudo a2ensite "$PROJECT_APACHE_CONF_SITE"
sudo a2dissite 000-default.conf
sudo service apache2 reload
check_status_and_exit_if_error 8 "Could not enable $PROJECT_APACHE_CONF_FILE"

echo "127.0.0.1 $LOCAL_PROJECT_URL" | sudo tee --append "$HOSTS_FILE" > /dev/null
check_status_and_exit_if_error 9 "Could not add $LOCAL_PROJECT_URL to $HOSTS_FILE"

exit_with_message_and_code 0 "Script complete"