exit_with_message_and_code () {
    local STATUS="$1"
    MESSAGE="$2"
    echo "$MESSAGE. Exiting with status $STATUS."
    exit "$STATUS"
}

check_status_and_exit_if_error () {
    local STATUS="$?"
    if [ "$STATUS" -ne 0 ]; then
        exit_with_message_and_code "$1" "$2"
    fi
}